using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patch 
{
    public HelpURLAttribute _id;
    public string image;
    public string corX;
    public string corY;
    public string createdAt;
    public string updatedAt;

}
