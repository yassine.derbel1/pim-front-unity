using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
//using Unity.Plastic.Newtonsoft.Json;
//using Unity.Plastic.Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class upload : MonoBehaviour
{
	[SerializeField] private string authentcationEndpoint = "http://192.168.80.1:3000/";
	 private string pram  ;
	//[SerializeField] private string paramId;

	public RawImage importImage;
	private string pdfFileType;
	private string pathone;
	public Button importButton;
	public Button ArCamera;
	[SerializeField] private Button uploadButton;
	private Texture2D uwrTextureyy;


	public void OnLoginClick2()
	{
		StartCoroutine(ImportImage());
	}
	public void ARCamera()
	{
		StartCoroutine(Calculcordonne());
	}
	public void OnUpload()
	{
		StartCoroutine(Upload());
	}

	private IEnumerator ImportImage()
	{
		pdfFileType = NativeFilePicker.ConvertExtensionToFileType("jpg"); // Returns "application/pdf" on Android and "com.adobe.pdf" on iOS
		Debug.Log("pdf's MIME/UTI is: " + pdfFileType);
		if (NativeFilePicker.IsFilePickerBusy())
			yield return null;
		NativeFilePicker.Permission permission = NativeFilePicker.PickFile((path) =>
		{
			if (path == null)
				Debug.Log("Operation cancelled");
			else
			{
				Debug.Log("Picked file: " + path);
				pathone = path;
				StartCoroutine(LoadImage(path));
				HideButton();

			}
		}, new string[] { pdfFileType });

		Debug.Log("Permission result: " + permission);




	}
	void HideButton()
	{
		importButton.gameObject.SetActive(false);
	}

	private IEnumerator LoadImage(string path)
	{

		using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path))
		{
			yield return uwr.SendWebRequest();
			if (uwr.isNetworkError || uwr.isHttpError)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				var uwrTexture = DownloadHandlerTexture.GetContent(uwr);
				importImage.texture = uwrTexture;
				uwrTextureyy = uwrTexture;


			}
		}
	}
	IEnumerator Upload()
	{
		Debug.Log(pathone);
		
		WWWForm form = new WWWForm();
		byte[] bytes = uwrTextureyy.EncodeToPNG();

		form.AddBinaryData("image", bytes, @"" + pathone, "image/jpg");

		//form.AddBinaryData("image", File.ReadAllBytes(pathone), "patch.jpg");
		//form.AddField("userId", "17ac4c482dcdd");

		UnityWebRequest www = UnityWebRequest.Post(authentcationEndpoint + "patch", form);

		yield return www.SendWebRequest();

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			//Debug.Log("Form upload complete! " + www.downloadHandler.text);
			Debug.Log("Form upload complete! " + www.downloadHandler.text);
			PlayerPrefs.SetString("_id", www.downloadHandler.text);
			string id = www.downloadHandler.text;


			
			Patch patch = new Patch();
			
			//var response = JObject.Parse(www.downloadHandler.text);
			//patch = JsonConvert.DeserializeObject<Patch>(response.SelectToken("imageCreated"));


			string json = www.downloadHandler.text;


			//patch = JsonUtility.FromJson<Patch>(json);

			//Debug.Log(patch._id);
			//Debug.Log(patch.image);
			

		}
	}

	IEnumerator Calculcordonne()
	{

		//byte[] myData = System.Text.Encoding.UTF8.GetBytes(paramId);
		//byte[] id = System.Text.Encoding.UTF8.GetBytes(paramId);
		//string json = " {\"id\":"  + paramId +" }";

		//byte[] myData = System.Text.Encoding.UTF8.GetBytes(paramId);
		//byte[] myData = System.Text.Encoding.UTF8.GetBytes("");

		//paramId = System.Uri.UnescapeDataString(PlayerPrefs.GetString("_id"));
		string param = (PlayerPrefs.GetString("_id")); //returns ".Net Framework"


		Console.WriteLine(param.Replace("\"", "") + "******************************");
		string id1 = param.Replace("\"", "");

		string haha = authentcationEndpoint + "patch/" + HttpUtility.UrlDecode(id1);
		Debug.Log(haha + "//////////////********88888888888888888888888******");

		UnityWebRequest request = UnityWebRequest.Put(haha, "hello");
		var handler = request.SendWebRequest();

		float startTime = +Time.deltaTime;
		while (!handler.isDone)
		{
			if (startTime > 10.0f)
			{
				break;
			}
			yield return null;
		}
		if (request.result == UnityWebRequest.Result.Success)
		{
			if (request.downloadHandler.text != "ERROR detect�e!")
			{
				//ArCamera.interactable = true;
				User reternedUser = JsonUtility.FromJson<User>(request.downloadHandler.text);
				//alerText.text = "Welcome" + reternedUser.username;
				LoadnextScene();

			}
			else
			{
				//alerText.text = "Invalid credentials";
				ArCamera.interactable = true;
			}
			Debug.Log(request.downloadHandler.text);
		}
		else
		{

			Debug.Log("Unable to connect to the serve..." );
			//ArCamera.interactable = true;

		}
		//Debug.Log($"{paramId}");


		yield return null;
	}
		private static void LoadnextScene()
	{
		SceneManager.LoadScene(2);
	}


	/*
	void Start()
	{
		pdfFileType = NativeFilePicker.ConvertExtensionToFileType("jpg"); // Returns "application/pdf" on Android and "com.adobe.pdf" on iOS
		Debug.Log("pdf's MIME/UTI is: " + pdfFileType);

	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			// Don't attempt to import/export files if the file picker is already open
			if (NativeFilePicker.IsFilePickerBusy())
				return;

			if (Input.mousePosition.x < Screen.width / 3)
			{
				// Pick a PDF file
				NativeFilePicker.Permission permission = NativeFilePicker.PickFile((path) =>
				{
					if (path == null)
						Debug.Log("Operation cancelled");
					else
						Debug.Log("Picked file: " + path);
				}, new string[] { pdfFileType });

				Debug.Log("Permission result: " + permission);
			}
			else if (Input.mousePosition.x < Screen.width * 2 / 3)
			{
#if UNITY_ANDROID
				// Use MIMEs on Android
				//string[] fileTypes = new string[] { "image/*", "video/*" };
				string[] fileTypes = new string[] { "image/*" };
#else
			// Use UTIs on iOS
			string[] fileTypes = new string[] { "public.image", "public.movie" };
#endif

				// Pick image(s) and/or video(s)
				NativeFilePicker.Permission permission = NativeFilePicker.PickMultipleFiles((paths) =>
				{
					if (paths == null)
						Debug.Log("Operation cancelled");
					else
					{
						for (int i = 0; i < paths.Length; i++)
							Debug.Log("Picked file: " + paths[i]);
					}
				}, fileTypes);

				Debug.Log("Permission result: " + permission);
			}
			else
			{
				// Create a dummy text file
				string filePath = Path.Combine(Application.temporaryCachePath, "test.txt");
				File.WriteAllText(filePath, "Hello world!");

				// Export the file
				NativeFilePicker.Permission permission = NativeFilePicker.ExportFile(filePath, (success) => Debug.Log("File exported: " + success));

				Debug.Log("Permission result: " + permission);
			}
		}
	}
	*/
}
