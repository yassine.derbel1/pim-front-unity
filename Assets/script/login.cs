using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class login : MonoBehaviour
{
    [SerializeField] private string authentcationEndpoint = "http://192.168.80.1:3000/";

    [SerializeField] private TMP_InputField userNameInputField;
    [SerializeField] private TMP_InputField passwordInputField;
    [SerializeField] private TextMeshProUGUI alerText;
    [SerializeField] private Button loginButton;


    // Update is called once per frame
    public void OnLoginClick()
    {
        StartCoroutine(tryLogin());
    }
    private IEnumerator tryLogin()
    {


        alerText.text = "Signing in...";
        loginButton.interactable = false;

        string username = userNameInputField.text;
        string password = passwordInputField.text;
        if (username.Length < 3 || username.Length > 24)
        {
            alerText.text = "invalid username";
            loginButton.interactable = true;
            yield break;
        }
        if (password.Length < 3 || password.Length > 24)
        {
            alerText.text = "invalid username";
            loginButton.interactable = true;
            yield break;
        }

        WWWForm form = new WWWForm();
        form.AddField("userName", username);
        form.AddField("password", password);


        UnityWebRequest request = UnityWebRequest.Post(authentcationEndpoint + "user/login", form);
        var handler = request.SendWebRequest();

        float startTime = +Time.deltaTime;
        while (!handler.isDone)
        {
            if (startTime > 10.0f)
            {
                break;
            }
            yield return null;
        }
        if (request.result == UnityWebRequest.Result.Success)
        {
            if (request.downloadHandler.text != "userName ou mot de passe n est pas valide ou compte non valide")
            {
                loginButton.interactable = false;
                User reternedUser = JsonUtility.FromJson<User>(request.downloadHandler.text);
                //alerText.text = "Welcome" + reternedUser.username;
                LoadnextScene();

            }
            else
            {
                alerText.text = "Invalid credentials";
                loginButton.interactable = true;
            }
            Debug.Log(request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Unable to connect to the serve...");
            loginButton.interactable = true;

        }
        Debug.Log($"{username}:{password}");


        yield return null;

    }
    private static void LoadnextScene()
    {
        SceneManager.LoadScene(1);
    }
}
